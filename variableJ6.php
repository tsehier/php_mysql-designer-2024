<input id="<?php echo $type['nomType']; ?>" type="radio" class="form-check-input" name="type" value="<?php echo $type['idType'] ?>" <?php if($type['idType']=='3'){
                        echo 'checked';
                    }?>>

                    <select name="ville" id="ville" class="form-control">
                        <?php foreach($villes as $ville): ?>
                            <option 
                                value="<?php echo $ville['idVille'] ?>" <?php if($ville['idVille']=='2'){
                                    echo 'selected';
                                } ?>
                            >
                                <?php echo $ville['nomVille'].' ('.$ville['codePostal'].')';?>
                            </option>
                        <?php endforeach; ?>    
                    </select>