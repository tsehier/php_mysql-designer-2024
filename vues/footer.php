<footer class="container-fluid bg-light">
        <div class="footer-menu container d-flex justify-content-between align-items-center border-bottom mb-4 pt-3">
            <div class="logo">
                <a href="" class="logo-link text-secondary-emphasis text-decoration-none fs-4"><i class="bi bi-tag-fill me-2" style="display:inline-block;transform: scaleX(-1);"></i><span class="fw-bold">Ticket</span>immobilier</a>
            </div>
            <nav class="navbar navbar-expand-lg">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="">NOS BIENS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">LOCALISER NOS BIENS</a>
                    </li>
                    <li class="nav-item bg-warning">
                        <a class="nav-link text-white" href=""><i class="bi bi-messenger me-1"></i>Message us</a>
                    </li>
                    <li class="nav-item">
                        <a href="/?destination=formulaire" class="nav-link">Ajouter un bien</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="footer-informations container d-flex justify-content-end">
            <div style="width: 33%;">
                <p>Tel: 05 61 30 38 69</p>
                <p>Email : contact@ticket-immo.fr</p>
                <p><a href="" class="text-decoration-none text-dark">Mentions Légales</a></p>
            </div>
        </div>
    </footer>
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>