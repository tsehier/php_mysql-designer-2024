<?php 


include('header.php'); 
?>
  
    <section class="hero">
        <img src="<?php echo config::$image_dir . 'hero.jpg' ?>" alt="">
    </section>
    <section class="container position-relative">
        <div class="searchForm p-4 bg-white position-absolute start-0 end-0 shadow" style="top: -40px">
            <form action="" class="form d-block">
                <div class="form-group d-flex gap-2">
                    <select name="" id="" class="form-control">
                        <option value="">Louer ou vendre</option>
                        <option value="">Louer</option>
                        <option value="">Vendre</option>
                    </select>
                    <select name="" id="" class="form-control">
                        <option value="">Type de bien</option>
                    </select>
                    <input type="text" placeholder="Ville..." class="form-control" name="ville">
                    <input type="text" placeholder="Prix maximum..." name="prixMax" class="form-control">
                    <button class="bg-warning text-white" style="border: none;">RECHERCHER</button>
                </div>
            </form>
        </div>
        <div class="logements d-flex flex-wrap gap-4">
            <!--
                Parcours l'ensemble des éléments du tableau logement
                Foreach prend en paramètre un tableau,
                chaque itération est stocké dans une variable ($logement)
                il en plus possible de récupérer la clé ($ref)

                tableau associatif =  { clé => valeur }
            -->
            <?php foreach($logements as $logement):
                if(isset($logement['images'])){
                    $maPremiereImage = $logement['images'][0]['lien'];
                    $maDescriptionImage = $logement['images'][0]['alt'];
                }else{
                    $maPremiereImage = 'not-found.jpg';
                    $maDescriptionImage = "Pas d'image";
                }
                $descriptionLogement =  ucfirst($logement['description']) ;
                $prix = $logement['prix'];
                $ville = strtoupper($logement['nomVille']);
                $location = ($logement['nomOffre']=='location')?'/mois': '';
                
            ?>
            <div class="logement bg-light">
                <div class="logement__img">
                    <img src="<?php echo config::$image_dir . $maPremiereImage ?>" alt="<?php echo $maDescriptionImage ?>">
                </div>
                <div class="logement_content px-3">
                    <p>Ref : <?php echo $logement['idLogement'] ?></p>
                    <p><?php echo $descriptionLogement?></p>
                    <p>
                        <a href="/?destination=maison&reference=<?php echo $logement['idLogement'] ?>" class="text-decoration-none text-warning d-flex align-items-center">En savoir plus<i class="bi bi-arrow-right ms-2"></i></a>
                    </p>
                </div>
                <div class="logement__bottom border-top border-warning px-3 d-flex justify-content-between pt-2">
                    <p><i class="bi bi-tag-fill me-2" style="display:inline-block;transform: scaleY(-1);"></i><?php echo $prix ?> €<?php echo $location ?></p>
                    <p class="text-warning fw-bold"><?php echo $ville ?></p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php include('footer.php'); ?>