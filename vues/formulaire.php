<?php

include_once('header.php');
?>

<section class="container">
    <div class="row">
        <div class="col">

            <?php if(isset($_SESSION['errors'])): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php foreach($_SESSION['errors'] as $errorLine): ?>
                            <li><?php echo $errorLine; ?></li>
                        <?php endforeach; ?>    
                    </ul>
                </div>
                <?php unset($_SESSION['errors']); ?>
            <?php endif; ?> 

            <form action="/?destination=envoiFormulaire" method="POST" class="form">
                <div class="form-group">
                    <label for="nom" class="form-label">Nom du bien</label>
                    <input id="nom" type="text" class="form-control" name="nom">
                </div>
                <div class="form-group">
                    <label for="prix" class="form-label">Prix du bien</label>
                    <input id="prix" type="number" class="form-control" name="prix">
                </div>
                <div class="form-group">
                    <label for="superficie" class="form-label">Superficie du bien</label>
                    <input id="superficie" type="number" class="form-control" name="superficie">
                </div>
                <div class="form-group">
                    <label for="description">Description du bien</label>
                    <textarea id="description" name="description" cols="30" rows="10" class="form-control"></textarea>
                </div>

                <p>Quel type de bien ?</p>
               
                <?php foreach($types as $type): ?>
           
                <div class="form-check form-check-inline">
                    <input id="<?php echo $type['nomType']; ?>" type="radio" class="form-check-input" name="type" value="<?php echo $type['idType'] ?>">
                    <label for="<?php echo $type['nomType']; ?>" class="form-check-label"><?php echo ucfirst($type['nomType']); ?></label>
                </div>

                <?php endforeach; ?>

                <p class="mt-3">Que proposez-vous ?</p>

                <?php foreach($offres as $offre): ?>
                <div class="form-check form-check-inline">
                    <input 
                        id="<?php echo $offre['nomOffre']; ?>"
                        type="radio" class="form-check-input"
                        name="offre"
                        value="<?php echo $offre['idOffre'] ?>" 
                    >
                    <label
                        for="<?php echo $offre['nomOffre']; ?>"
                        class="form-check-label"
                    >
                        <?php echo ucfirst($offre['nomOffre']); ?>
                    </label>
                </div>
                <?php endforeach; ?>   
                
                <div class="form-group">
                    <label for="ville">Ville du bien</label>
                    <select name="ville" id="ville" class="form-control">
                        <?php foreach($villes as $ville): ?>
                            <option 
                                value="<?php echo $ville['idVille'] ?>"
                            >
                                <?php echo $ville['nomVille'].' ('.$ville['codePostal'].')';?>
                            </option>
                        <?php endforeach; ?>    
                    </select>
                </div>

                <div class="form-group my-2">
                    <input type="submit" value="Envoyer" class="btn btn-outline-success">
                </div>

            </form>
        </div>
    </div>
</section>

<?php
include('footer.php');