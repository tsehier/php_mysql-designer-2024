<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <title>Document</title>
</head>
<body>
<header>
        <div class="container d-flex justify-content-between align-items-center">
            <div class="logo">
                <a href="" class="logo-link text-secondary-emphasis text-decoration-none fs-4"><i class="bi bi-tag-fill me-2" style="display:inline-block;transform: scaleX(-1);"></i><span class="fw-bold">Ticket</span>immobilier</a>
            </div>
            <nav class="navbar navbar-expand-lg">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="">NOS BIENS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">LOCALISER NOS BIENS</a>
                    </li>
                    <li class="nav-item bg-warning">
                        <a class="nav-link text-white" href=""><i class="bi bi-messenger me-1"></i>Message us</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>