<?php

session_start();

include('config.php');

include('controleurs/publicControleur.php');

if(isset($_GET['destination'])){
    $destination = $_GET['destination'];
}else{
    $destination = '';
}

switch ($destination){
    case 'maison':
        publicControleur::maison();
        break;
    case 'formulaire':
        publicControleur::formulaire();
        break;
    case 'envoiFormulaire':
        publicControleur::envoiFormulaire();
        break;
    case 'successFormulaire':
        publicControleur::successFormulaire();
        break;
    default:
        publicControleur::accueil();
}
