<?php


class Types extends Database{

    public function getAllTypes(){
        $requete = $this->connect()->prepare('SELECT * FROM types');
        $requete->execute();
        $reponse = $requete->fetchAll();
        return $reponse;
    }

}