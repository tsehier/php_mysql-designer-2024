<?php 

class Logements extends Database{

    public $nom;
    public $prix;
    public $superficie;
    public $description;
    public $ville;
    public $offre;
    public $type;

    public function saveLogement(){
        $requete = $this->connect()->prepare('INSERT INTO logements (nom, prix, superficie, description, idVille, idOffre, idType) VALUES (:nom, :prix, :superficie, :description, :idVille, :idOffre, :idType)');
        $requete->execute([
            'nom' => $this->nom,
            'prix' => $this->prix,
            'superficie' => $this->superficie,
            'description' => $this->description,
            'idVille' => $this->ville,
            'idOffre' => $this->offre,
            'idType' => $this->type,
        ]);
    }

    public function getAllLogements(){
        $requete = $this->connect()
        ->prepare('SELECT * FROM logements 
        INNER JOIN villes ON logements.idVille = villes.idVille 
        INNER JOIN offres ON logements.idOffre = offres.idOffre
        INNER JOIN types ON logements.idType = types.idType');
        $requete->execute();
        $response = $requete->fetchAll();
        return $response;
    }

    public function getLogementUnique($idLogement){
        $requete = $this->connect()
        ->prepare('SELECT * FROM logements
        INNER JOIN offres ON logements.idOffre = offres.idOffre
        INNER JOIN types ON logements.idType = types.idType
        INNER JOIN villes ON logements.idVille = villes.idVille
        WHERE idLogement = :idLogement');
        $requete->execute([
            'idLogement' => $idLogement
        ]);
        $response = $requete->fetch();
        return $response;
    }
}