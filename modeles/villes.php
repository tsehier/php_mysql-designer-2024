<?php

class Villes extends Database{

    public function getAllVilles(){
        $requete = $this->connect()->prepare('SELECT * FROM villes');
        $requete->execute();
        $reponse = $requete->fetchAll();
        return $reponse;
    }

}