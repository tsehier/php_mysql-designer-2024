<?php
include('modeles/database.php');
include('modeles/types.php');
include('modeles/offres.php');
include('modeles/villes.php');
include('modeles/logements.php');

class publicControleur{

    public static function accueil(){
        $logements = new Logements();
        $logements = $logements->getAllLogements();

        include('vues/accueil.php');
    }

    public static function maison(){
        
        // On récupère la valeur de notre variable "reference" http présente dans l'URL
        $idLogement = $_GET['reference'];

        $logementSingle = new Logements();
        $logementSingle = $logementSingle->getLogementUnique($idLogement);

        //$logementSingle = $logements[$ref2];

        if($logementSingle['nomOffre']==='location'){
            $quittance = '€/mois';
        }else{
            $quittance = '€';
        }

        if(isset($logementSingle['image'])){
            $maPremiereImage = $logementSingle['image'][0];
        }else{
            $maPremiereImage = 'not-found.jpg';
            $maPremiereDescription = 'Pas d\'image';
        }

        include('vues/maison.php');
    }

    public static function formulaire(){
        $types = new Types();
        $types = $types->getAllTypes();

        $offres = new Offres();
        $offres = $offres->getAllOffres();

        $villes = new Villes();
        $villes = $villes->getAllVilles();

        include('vues/formulaire.php');
    }

    public static function envoiFormulaire(){
        $errors = [];

        if(empty($_POST['nom'])){
            $errors[] = "Vous n'avez pas saisi de Nom";
        }
        if(empty($_POST['prix'])){
            $errors[] = "Vous n'avez pas saisi de Prix"; 
        }
        if(empty($_POST['superficie'])){
            $errors[] = "Vous n'avez pas saisi de superficie"; 
        }
        if(empty($_POST['description'])){
            $errors[] = "Vous n'avez pas saisi de description"; 
        }
        if(empty($_POST['ville'])){
            $errors[] = "Vous n'avez pas saisi de ville"; 
        }

        if(!isset($_POST['type'])){
            $errors[] = "Vous n'avez pas choisi de type"; 
        }
        if(!isset($_POST['offre'])){
            $errors[] = "Vous n'avez pas choisi d'offre"; 
        }

        if(!empty($errors)){
            $_SESSION['errors'] = $errors;
            header('Location: /?destination=formulaire');
        }

        $logement = new Logements();

        $logement->nom = $_POST['nom'];
        $logement->prix = $_POST['prix'];
        $logement->superficie = $_POST['superficie'];
        $logement->description = $_POST['description'];
        $logement->ville = $_POST['ville'];
        $logement->type = $_POST['type'];
        $logement->offre = $_POST['offre'];

        $logement->saveLogement();

        header('Location: /?destination=successFormulaire');

    }

    public static function successFormulaire(){
        include('vues/successFormulaire.php');
    }
}