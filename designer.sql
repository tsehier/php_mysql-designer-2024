-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 26 fév. 2024 à 16:34
-- Version du serveur : 8.2.0
-- Version de PHP : 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `designer`
--

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `idImage` int NOT NULL AUTO_INCREMENT,
  `nomImage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`idImage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `logements`
--

DROP TABLE IF EXISTS `logements`;
CREATE TABLE IF NOT EXISTS `logements` (
  `idLogement` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `prix` float NOT NULL,
  `superficie` float NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `idVille` int NOT NULL,
  `idType` int NOT NULL,
  `idOffre` int NOT NULL,
  PRIMARY KEY (`idLogement`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `logements`
--

INSERT INTO `logements` (`idLogement`, `nom`, `prix`, `superficie`, `description`, `idVille`, `idType`, `idOffre`) VALUES
(1, 'Grande maison Saint-Augustin', 2682, 157, 'À louer à Saint-Augustin, maison de ville meublée de 157m² avec 4 chambres dont une suite parentale de 27m², une grande pièce de vie de 60m² et un garage intégré. Jardin sans vis-à-vis de 60m² dont 20m² de terrasse bois accessible directement depuis la pièce de vie par deux grandes baies vitrées orientées sud et ouest.\r\nMaison très bien située, à 600m d’un arrêt de la ligne A du tramway, à 600m de la place de l’église Saint-Augustin, à 1200m du parc de Bourran.\r\nConstruction de 2019 très bien isolée, chauffage au sol, chaudière gaz avec panneau solaire pour l’eau chaude. Cuisine équipée avec électroménager récent, cellier avec lave-linge.\r\nMaison disponible à partir du 3 mars. Loyer de 2632€, plus 50€ de provisions de charges (contrat d’entretien de la chaudière et TOM). Pas de frais de mise en location.\r\n\r\nMontant estimé des dépenses annuelles d\'énergie pour un usage standard : 950€ par an. Prix moyens des énergies indexés sur l\'année 2023 (abonnements compris).', 2, 1, 2),
(2, 'A louer 2 pièces proche pont de la motte rouge', 740, 50, 'Appartement en très bon état,ensoleillé, proche tram et bus ,chambre côté jardin dans petite résidence de standing.', 1, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `logementsimages`
--

DROP TABLE IF EXISTS `logementsimages`;
CREATE TABLE IF NOT EXISTS `logementsimages` (
  `idLogement` int NOT NULL,
  `idImage` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `offres`
--

DROP TABLE IF EXISTS `offres`;
CREATE TABLE IF NOT EXISTS `offres` (
  `idOffre` int NOT NULL AUTO_INCREMENT,
  `nomOffre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`idOffre`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `offres`
--

INSERT INTO `offres` (`idOffre`, `nomOffre`) VALUES
(1, 'Vente'),
(2, 'Location');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `idType` int NOT NULL AUTO_INCREMENT,
  `nomType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`idType`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`idType`, `nomType`) VALUES
(1, 'Appartement'),
(2, 'Maison'),
(3, 'Terrain'),
(4, 'Garage');

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

DROP TABLE IF EXISTS `villes`;
CREATE TABLE IF NOT EXISTS `villes` (
  `idVille` int NOT NULL AUTO_INCREMENT,
  `nomVille` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `codePostal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`idVille`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `villes`
--

INSERT INTO `villes` (`idVille`, `nomVille`, `codePostal`) VALUES
(1, 'Saint-Lô', '50000'),
(2, 'Caen', '14000');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
