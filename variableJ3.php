<?php

function DireBonjour($prenom = null){
    $phrase = 'Bonjour';
    if($prenom!=null){
        $phrase .= ' '. $prenom;
    }
    $phrase .= ',';
    return $phrase;
}

function message(){
    return 'Il va bien' .'<br>';
}

function DireAuRevoir(){
    return 'Cordialement' . '<br>' . 'Super Site';
}

// echo DireBonjour('Philippe');
// echo '<br>';
// echo message();
// echo DireAuRevoir();



class Emailing{

    private $prenom;
    private $message;

    public function __construct($prenom = null){
        $this->prenom = $prenom;
    }

    private function DireBonjour(){
        $phrase = 'Bonjour';
        if($this->prenom!=null){
            $phrase .= ' '. $this->prenom;
        }
        $phrase .= ',';
        $this->message .= $phrase;
    }
    
    private function message(){
        $this->message .= 'Il va bien' .'<br>';
    }
    
    private function DireAuRevoir(){
        $this->message .= 'Cordialement' . '<br>' . 'Super Site';
    }

    public function genereMessage(){
        $this->DireBonjour();
        $this->message();
        $this->DireAuRevoir();
        return $this->message;
    }

}

$emailing = new Emailing();
echo $emailing->genereMessage();
echo "<br>";
echo "<br>";
$emailing2 = new Emailing('Pierre');
echo $emailing2->genereMessage();
echo "<br>";
echo "<br>";


$emailing3 = new Emailing('Paul');
echo $emailing3->genereMessage();




